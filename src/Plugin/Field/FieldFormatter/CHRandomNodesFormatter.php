<?php

namespace Drupal\ch_random_nodes\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'CH Random Nodes' formatter.
 *
 * @FieldFormatter(
 *   id = "ch_random_nodes_formatter",
 *   label = @Translation("Random Nodes Formatter"),
 *   description = @Translation("Display Random Nodes"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class CHRandomNodesFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Entity Type Manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => 'thumbnail',
      'num_nodes' => '3',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element['image_style'] = [
      '#title' => $this->t('Image Style'),
      '#type' => 'select',
      '#options' => chrn_image_style_options(),
      '#default_value' => $this->getSetting('image_style') ?: 'thumbnail',
    ];

    $element['num_nodes'] = [
      '#title' => $this->t('Number of Nodes'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('num_nodes') ?: '3',
    ];

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];
    $settings = $this->getSettings();
    $summary[] = $this->t('Image Style:@space', ['@space' => ' ']) . $settings['image_style'];
    $summary[] = $this->t('Number of Nodes:@space', ['@space' => ' ']) . $settings['num_nodes'];

    return $summary;

  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Verify langcode is set.
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    // Instantiate our arrays.
    $entityIDList = [];
    $nids = [];
    $elements = [];

    // Get our node limit from form settings.
    $numNodes = $this->getSetting('num_nodes');

    // Build our entity ID list.
    $entityIDList = array_column($items->getValue(), 'target_id');

    // Check our entity ID list is longer than our limit.
    if (count($entityIDList) > $numNodes) {

      // Shuffle our entity ID list for randomness.
      shuffle($entityIDList);

      // Get our limit of first array items.
      $nids = array_slice($entityIDList, 0, $numNodes);

    }
    if (count($entityIDList) <= $numNodes) {

      // We have less than our defined limit, show all nodes.
      $nids = $entityIDList;
    }

    // Loop through our elements to build output.
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $item) {

      // Get our image URI.
      $topicImageURI = $item->field_topic_image->entity->uri->value;

      // Get our Image Style URL.
      $topicImageURL = $this->entityTypeManager->getStorage('image_style')->load($this->getSetting('image_style'))->buildUrl($topicImageURI);

      // Check current entity ID matches a random entity ID.
      if (in_array($item->id(), $nids)) {

        // Build our elements for render.
        $elements[$delta] = [
          '#theme' => 'ch_random_nodes',
          '#nid' => $item->nid->value,
          '#title' => $item->title->value,
          '#image' => $topicImageURL,
          '#image_alt' => $item->field_topic_image->alt,
          '#bundle' => $item->bundle(),
          '#cache' => [
            'max-age' => '0',
          ],
        ];
      }
    }
    return $elements;
  }

}
